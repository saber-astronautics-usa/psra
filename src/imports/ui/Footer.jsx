import React from 'react';

export const Footer = () => (
    <footer className="footer">
      <div className="container">
        <span className="text-muted" >2020 - SpaceIntel - Terms - Privacy Policy</span>
      </div>
    </footer>
  );