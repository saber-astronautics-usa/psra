# List of Action Items

======================= MVP =======================

## 1 Bugs 
- Add Schema Button, modal has all fields disabled.

## 2 Restore schemas:     
mission type 
launch vehicle 
organisation 
news
status
power
payload
prop
images

## 3 Add/Edit Satellites

## 4 Validation
Add client side validation for Add Satellite and Add/Edit Schema (ensure no empty fields, correct types, cant delete schemaName, description, reference)

## 5 Settle on Name (Poll) and License Structure

## 6 Remove Dark theme hack
The Bootstrap dark theme is being overidden by the normal theme, we have added css (see !important's) to override this temporarily.

## 7 Design satellite baseball card
- Use react-flip-card, front side has general overview, rear has more detailed info.

====================================================



## 8 Deploy onto github so we can have propper issue tracking

## 9 Logins
Allow people to login and store who edited what, when.
